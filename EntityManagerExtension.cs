namespace Unity.Entities
{
	public static class EntityManagerExtension
	{
		public static EntityArchetype CreateArchetype<T0>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1, T2>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ), typeof( T2 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1, T2, T3>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ), typeof( T2 ), typeof( T3 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1, T2, T3, T4>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ), typeof( T2 ), typeof( T3 ), typeof( T4 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1, T2, T3, T4, T5>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ), typeof( T2 ), typeof( T3 ), typeof( T4 ), typeof( T5 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1, T2, T3, T4, T5, T6>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ), typeof( T2 ), typeof( T3 ), typeof( T4 ), typeof( T5 ), typeof( T6 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1, T2, T3, T4, T5, T6, T7>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ), typeof( T2 ), typeof( T3 ), typeof( T4 ), typeof( T5 ), typeof( T6 ), typeof( T7 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1, T2, T3, T4, T5, T6, T7, T8>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ), typeof( T2 ), typeof( T3 ), typeof( T4 ), typeof( T5 ), typeof( T6 ), typeof( T7 ), typeof( T8 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ), typeof( T2 ), typeof( T3 ), typeof( T4 ), typeof( T5 ), typeof( T6 ), typeof( T7 ), typeof( T8 ),
				typeof( T9 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ), typeof( T2 ), typeof( T3 ), typeof( T4 ), typeof( T5 ), typeof( T6 ), typeof( T7 ), typeof( T8 ),
				typeof( T9 ), typeof( T10 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ), typeof( T2 ), typeof( T3 ), typeof( T4 ), typeof( T5 ), typeof( T6 ), typeof( T7 ), typeof( T8 ),
				typeof( T9 ), typeof( T10 ), typeof( T11 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ), typeof( T2 ), typeof( T3 ), typeof( T4 ), typeof( T5 ), typeof( T6 ), typeof( T7 ), typeof( T8 ),
				typeof( T9 ), typeof( T10 ), typeof( T11 ), typeof( T12 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ), typeof( T2 ), typeof( T3 ), typeof( T4 ), typeof( T5 ), typeof( T6 ), typeof( T7 ), typeof( T8 ),
				typeof( T9 ), typeof( T10 ), typeof( T11 ), typeof( T12 ), typeof( T13 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ), typeof( T2 ), typeof( T3 ), typeof( T4 ), typeof( T5 ), typeof( T6 ), typeof( T7 ), typeof( T8 ),
				typeof( T9 ), typeof( T10 ), typeof( T11 ), typeof( T12 ), typeof( T13 ), typeof( T14 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ), typeof( T2 ), typeof( T3 ), typeof( T4 ), typeof( T5 ), typeof( T6 ), typeof( T7 ), typeof( T8 ),
				typeof( T9 ), typeof( T10 ), typeof( T11 ), typeof( T12 ), typeof( T13 ), typeof( T14 ), typeof( T15 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ), typeof( T2 ), typeof( T3 ), typeof( T4 ), typeof( T5 ), typeof( T6 ), typeof( T7 ), typeof( T8 ),
				typeof( T9 ), typeof( T10 ), typeof( T11 ), typeof( T12 ), typeof( T13 ), typeof( T14 ), typeof( T15 ), typeof( T16 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ), typeof( T2 ), typeof( T3 ), typeof( T4 ), typeof( T5 ), typeof( T6 ), typeof( T7 ), typeof( T8 ),
				typeof( T9 ), typeof( T10 ), typeof( T11 ), typeof( T12 ), typeof( T13 ), typeof( T14 ), typeof( T15 ), typeof( T16 ), typeof( T17 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ), typeof( T2 ), typeof( T3 ), typeof( T4 ), typeof( T5 ), typeof( T6 ), typeof( T7 ), typeof( T8 ),
				typeof( T9 ), typeof( T10 ), typeof( T11 ), typeof( T12 ), typeof( T13 ), typeof( T14 ), typeof( T15 ), typeof( T16 ), typeof( T17 ), typeof( T18 ) );
		}

		public static EntityArchetype CreateArchetype<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19>(this EntityManager entityManager)
		{
			return entityManager.CreateArchetype( typeof( T0 ), typeof( T1 ), typeof( T2 ), typeof( T3 ), typeof( T4 ), typeof( T5 ), typeof( T6 ), typeof( T7 ), typeof( T8 ),
				typeof( T9 ), typeof( T10 ), typeof( T11 ), typeof( T12 ), typeof( T13 ), typeof( T14 ), typeof( T15 ), typeof( T16 ), typeof( T17 ), typeof( T18 ), typeof( T19 ) );
		}
	}
}

