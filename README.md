### Simple extension Method to provide generic overloads to Unity.Entities.EntityManager.CreateArchetype()
supported are overloads for up to 20 components 

##### usage with extension: 
```
EntityArchetype player = entityManager.CreateArchetype<Position, Rotation, PlayerInput, Health, TransformMatrix>();
```


##### usage without extension: 
```
EntityArchetype player = entityManager.CreateArchetype( typeof( Position ), typeof( Rotation ), typeof( PlayerInput ), typeof( Health ), typeof( TransformMatrix ) );
```